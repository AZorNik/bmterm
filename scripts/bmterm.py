
class arg :
	def __init__(self) :
		self.str = ""
		self.ar = False
		self.ar_n = 0

	def __str__(self) :
		res = self.str
		if self.ar :
			res += ":[" + str(self.ar_n) + "]"
		return res

class parser :
	def __init__(self) :
		self.name = ""
		self.args = []

	def __str__(self) :
		args_str = ""
		for arg in self.args :
			args_str += str(arg) + " "
		return self.name + "(" + args_str + ")"

class exe :
	def __init__(self) :
		self.result = ""
		self.name = ""
		self.args = []
		self.decl = ""

	def __str__(self) :
		args_str = ""
		for arg in self.args :
			args_str += str(arg) + " "
		return self.name + "(" + args_str + ") -> " + self.result


class cmd :
	def __init__(self) :
		self.parser = parser()
		self.exe = exe()

	def __str__(self) :
		return "[ " + str(self.parser) + ", " + str(self.exe) + " ]"


start_part = '//*'
mark = 'bmterm'
cmd_mark = 'cmd'

parser_result = "bmterm::Result"
parser_args = "std::string_view input"
parser_prefix = "parser_"

dir_separator = '/'

gen_files_dir = "./bmterm_gen/"
gen_files_dir = gen_files_dir.lstrip()
if len(gen_files_dir) != 0 and gen_files_dir[-1] != dir_separator :
	gen_files_dir += dir_separator

table_file_name = "bmterm_table.hpp"
parsers_cpp_file_name = "bmterm_parsers.cpp"

bmterm_header_file_name = "azn/bmterm/bmterm.hpp"
table_includes = [bmterm_header_file_name]
parser_cpp_includes = [bmterm_header_file_name]

