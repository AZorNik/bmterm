import sys
import os
import bmterm

def find_mark(line, mark) :
	line = line.lstrip()
	ok = line.startswith(mark)
	if ok :
		line = line[len(mark):]
	return [ok, line]

def find_cmd(line) :
	cmd = bmterm.parser()
	line = line.lstrip()
	end_cmd = line.find('"', 1)
	ok = line.startswith('"') and end_cmd > 0
	if ok  and end_cmd:
		w = line[1:end_cmd].split()
		cmd.name = w[0]
		for arg_str in w[1:] :
			arg = bmterm.arg()
			ar_size_b = arg_str.find('[')
			if ar_size_b < 0 :
				arg.str = arg_str
			else :
				arg.str = arg_str[:ar_size_b]
				ar_size_e = arg_str.find(']')
				arg.ar = True
				arg.ar_n = str(arg_str[ar_size_b + 1 : ar_size_e])
			cmd.args.append(arg)

	return [ok, cmd]


def parse_executor(line) :
	exe = bmterm.exe()
	args_begin = line.find('(')
	args_end = line.find(')')

	exe.decl = line[:args_end + 1] + ";"

	ok = args_begin > 0 and args_end > 0 and args_end > args_begin
	if ok :
		ba = line[:args_begin].split()
		ok = len(ba) == 2
		if ok :
			exe.name = ba[1]
			exe.result = ba[0]

	if ok :
		ar = False
		args = line[args_begin + 1:args_end].split(',')
		for arg in args :
			if ar :
				ar = False
			else :
				arg = arg.replace("void", "").lstrip();
				if len(arg) > 0 :
					arg_type = arg[:arg.rfind(' ')]
					if arg_type.find('*') > 0 :
						ar = True
					exe_arg = bmterm.arg()
					exe_arg.str = arg_type.replace('*', '')
					exe_arg.ar = ar
					exe.args.append(exe_arg)

	return [ok, exe]


def look_up(file_name) :
	cmds = []
	with open(file_name) as f :
		line = f.readline()

		while line :
			cmd = bmterm.cmd()
			[ok, line] = find_mark(line, bmterm.start_part)

			if ok :
				[ok, line] = find_mark(line, bmterm.mark)

			if ok :
				[ok, line] = find_mark(line, bmterm.cmd_mark)

			if ok :
				[ok, cmd.parser] = find_cmd(line)

			if ok :
				line = f.readline()
				if line :
					[ok, cmd.exe] = parse_executor(line)
					# and bmterm.isOk() and message if not
					if ok and len(cmd.exe.args)==len(cmd.parser.args) :
						cmds.append(cmd)

			line = f.readline()
	return cmds

def code_decl_arg_ar(arg_name, arg_type, cap, offset) :
	code = offset + "constexpr std::size_t " + arg_name + "Capacity = " + str(cap) + ";\n"
	code += offset + arg_type + " " + arg_name + "[" + arg_name + "Capacity];\n"
	code += offset + "std::size_t " + arg_name + "Size;\n"

	return code

def code_decl_arg(arg_name, arg_type, offset) :
	code = offset + arg_type + " " + arg_name + ";\n"

	return code

def code_parse_array(arg_name, offset) :
	arg_n = arg_name + "Size"
	#code = offset + "res = res && bmterm::parseArraySize(input, " + arg_n + ");\n"
	code = offset + "res = res && bmterm::parseArray(input, " + arg_name + ", " + arg_name + "Capacity, " + arg_n + ");\n"
	code += offset + "bmterm::skipArray(input);\n"

	return code

def code_parse_arg(arg_name, offset) :
	code = offset + "res = res && bmterm::parse(input, " + arg_name + ");\n"
	code += offset + "bmterm::skipToken(input);\n"

	return code

def code_call_executor(cmd, offset) :
	code = offset + "if (res) {\n"
	code += offset * 2;
	if cmd.exe.result != "void":
		code += cmd.exe.result + " parser_res = "
	code += cmd.exe.name
	code += "("
	first = True
	for i in range(1,len(cmd.exe.args)+1) :
		if first :
			first = False
		else:
			code += ", "
		code += "arg" + str(i)
		if cmd.exe.args[i - 1].ar :
			code += ", arg" + str(i) + "Size"
	code += ");\n"
	code += offset + '}\n'

	return code


def code_parser_head(cmd, offset) :
	code = bmterm.parser_result
	code += " " + bmterm.parser_prefix
	code += cmd.exe.name
	code += "("
	code += bmterm.parser_args
	code += ") {\n"

	code += offset + bmterm.parser_result + " res = true;\n"
	code += "\n"

	return code


def code_parser_tail(cmd, offset) :
	code = "\n" + offset + "return res;\n}\n\n"
	return code


def rm_const_and_ref(line) :
	return line.replace("const", "").replace("&", "").lstrip()

def gen_parser(cmd) :

	offset = '\t'
	parser = code_parser_head(cmd, offset)

	if len(cmd.exe.args) > 0 :
		for j in range(len(cmd.exe.args)) :
			i = int(cmd.parser.args[j].str) - 1
			arg_type = rm_const_and_ref(cmd.exe.args[i].str)
			arg_name = "arg" + str(i + 1)
			if cmd.exe.args[i].ar :
				parser += code_decl_arg_ar(arg_name, arg_type, cmd.parser.args[j].ar_n , offset)
			else :
				parser += code_decl_arg(arg_name, arg_type, offset)

		parser += '\n' * 2
		parser += offset + "bmterm::skipToken(input);\n\n"
		for j in range(len(cmd.exe.args)) :
			i = int(cmd.parser.args[j].str) - 1
			arg_name = "arg" + str(i + 1)
			if cmd.exe.args[i].ar :
				parser += code_parse_array(arg_name, offset)
			else :
				parser += code_parse_arg(arg_name, offset)
			parser += '\n'

		parser += '\n'

	parser += code_call_executor(cmd, offset)

	parser += code_parser_tail(cmd, offset)

	return parser


def code_add_include(inc) :
	code = "#include \"" + inc + "\"\n"
	return code


def code_decl_executor(cmd) :
	return cmd.exe.decl

def gen_parsers_cpp(cmds) :
	cpp = ""
	for inc in bmterm.parser_cpp_includes :
		cpp += code_add_include(inc)

	cpp += "\nusing namespace azn;\n\n"

	for cmd in cmds :
		cpp += code_decl_executor(cmd) + "\n"
	cpp += "\n"

	for cmd in cmds :
		cpp += gen_parser(cmd)

	return cpp


def code_decl_parser(cmd) :
	code = "azn::" + bmterm.parser_result
	code += " " + bmterm.parser_prefix
	code += cmd.exe.name
	code += "("
	code += bmterm.parser_args
	code += ");\n"

	return code


def gen_table(cmds) :
	table = "#pragma once\n\n"

	for inc in bmterm.table_includes :
		table += code_add_include(inc)

	table += "\n"

	for cmd in cmds :
		table += code_decl_parser(cmd)

	table += "\n"

	table += "\nusing namespace azn;\n\n"
	table += "const bmterm::Command CmdTable[] = {\n"

	tab = {}
	for cmd in cmds :
		tab.update({cmd.parser.name : cmd.exe.name})
	for r in sorted(tab.items()) :
		table += "\t{\"" + r[0] + "\", " + bmterm.parser_prefix + r[1] + "},\n"
	table += "};\n\n"

	table += "constexpr size_t CmdQty = sizeof(CmdTable) / sizeof(CmdTable[0]);\n\n"

	return table


if __name__ == "__main__" :
	cmds = []
	for i in sys.argv[1:] :
		cmds += look_up(i)

	if not os.path.exists(bmterm.gen_files_dir) :
		os.mkdir(bmterm.gen_files_dir)
	os.chdir(bmterm.gen_files_dir)

	tab_file = open(bmterm.table_file_name, 'w')
	tab_file.write(gen_table(cmds))

	parsers_cpp = open(bmterm.parsers_cpp_file_name, 'w')
	parsers_cpp.write(gen_parsers_cpp(cmds))

