#include "./parser.hpp"

using namespace std;

namespace azn::bmterm {

TokenPos findToken(string_view input, size_t start) {
	auto tokenBegin = input.find_first_not_of(' ', start);
	auto tokenEnd = input.find_first_of(' ', tokenBegin);

	if (tokenBegin == string_view::npos) {
		tokenBegin = input.length();
	}

	if (tokenEnd == string_view::npos) {
		tokenEnd = input.length();
	}

	return {tokenBegin, tokenEnd};
}

void skipToken(string_view& input) {
	auto tokenPos = findToken(input);
	input.remove_prefix(tokenPos.end);
}

bool parse(string_view input, char& val) {
	bool ok;

	auto tokenPos = findToken(input);
	if ((tokenPos.end - tokenPos.begin) == 1) {
		val = input[tokenPos.begin];
		ok = true;
	}
	else {
		ok = false;
	}

	return ok;
}

bool parse(string_view input, bool& val) {
	bool ok;

	auto tokenPos = findToken(input);
	if ((tokenPos.end - tokenPos.begin) == 1) {
		char ch = input[tokenPos.begin];
		if ((ch == 'y') || (ch == 'Y') || (ch == 't') || (ch == 'T') || (ch == '1')){
			val = true;
			ok = true;
		}
		else if ((ch == 'n') || (ch == 'N') || (ch == 'f') || (ch == 'F') || (ch == '0')){
			val = false;
			ok = true;
		}
		else {
			ok = false;
		}
	}
	else {
		ok = false;
	}

	return ok;
}

TokenPos findArray(string_view input) {
	auto arrayBegin = input.find('[');
	auto arrayEnd = input.find(']');

	return {arrayBegin, arrayEnd};
}

bool isTokenPosOk(const TokenPos& pos) {
	return ((pos.begin != string_view::npos) && (pos.end != string_view::npos)
			&& (pos.end > pos.begin));
}

void skipArray(string_view& input) {
	auto arrayPos = findArray(input);
	if (isTokenPosOk(arrayPos)) {
		input.remove_prefix(arrayPos.end + 1);
	}
}

}
