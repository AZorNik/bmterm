#include "bmterm.hpp"
#include <algorithm>

using namespace azn::bmterm;
using namespace std;


Terminal::Terminal(ReadSymbol readSym, WriteSymbol writeSym)
		: readSymbol(readSym)
		, writeSymbol(writeSym)
	{
	writeSymbol(':');
}

void Terminal::loadTable(const Command* table, size_t tableSize) {
	commandTable = table;
	cmdQty = tableSize;
}

void Terminal::process(void) {
	Symbol sym;
	while (readSymbol(sym)) {
		processSymbol(sym);
	}
}

void Terminal::processSymbol(Symbol sym) {
	switch (sym) {
		case NewLine :
		case NewLine2 :
			parseCmd();
			break;

		default :
			addSymbol(sym);
	}
}

bool Terminal::addSymbol(Symbol sym) {
	//TODO check index
	inputString[inputStringIndex++] = sym;

	return true;
}

void Terminal::parseCmd(void) {
	if (inputStringIndex > 0) {
		string_view input {inputString, inputStringIndex};
		auto cmd = findCmd(input);

		if (cmd != commandTable + cmdQty) {
			cmd->parser(inputString);
		}
		else {
			print("Command not found\n");
		}

		inputStringIndex = 0;
	}
	writeSymbol(':');
}

const Command* Terminal::findCmd(std::string_view input) {
	auto spacePosition = input.find(' ');
	std::string_view cmd = input.substr(0, spacePosition);

	auto cmp = [](const Command& cmd, string_view sv) {
		auto cmpRes = sv.compare(cmd.name);
		return cmpRes > 0;
	};

	auto it = lower_bound(&commandTable[0], commandTable + cmdQty, cmd, cmp);
	if (cmd.compare(it->name) != 0){
		it = commandTable + cmdQty;
	}

	return it;
}

bool Terminal::print(bool b) {
	bool ok = b ? print("true") : print("false");
	return ok;
}

bool Terminal::print(char ch) {
	return writeSymbol(ch);
}

bool Terminal::print(const char* str) {
	bool ok = true;

	std::size_t i = 0;

	while (ok && (str[i] !='\0')) {
		ok = writeSymbol(str[i]);
		++i;
	}

	return ok;
}
