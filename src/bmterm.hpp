#pragma once
#include <cstddef>
#include <cstdint>
#include <string_view>
#include <charconv>

namespace azn::bmterm {

using Symbol = char;
template<unsigned N>
using String = Symbol[N+1];
using CmdId = size_t;

inline constexpr unsigned InputStringLengthMax = 80;
inline constexpr unsigned CommandLengthMax = 20;

using Result = bool;
using Parser = Result (*)(std::string_view cmd);

struct Command {
	String<CommandLengthMax> name {""};
	Parser parser {nullptr};
};

using ReadSymbol = bool (*)(Symbol& sym);
using WriteSymbol = bool (*)(const Symbol& sym);

class Terminal{
	public:

		Terminal(ReadSymbol readSym, WriteSymbol writeSym);
		void loadTable(const Command* table, size_t tableSize);
		void process(void);

		template<typename T
				, std::enable_if_t<std::is_integral_v<T>, bool> = true>
		bool print(T t, int base = 10) {
			char ar[20];
			auto res = std::to_chars(&ar[0], ar + 19, t, base);
			bool ok = (res.ec != std::errc::invalid_argument) && (res.ec != std::errc::result_out_of_range);

			if (ok) {
				*res.ptr = '\0';
				if (base != 10) {
					if (base == 16) {
						print("0x");
					}
					else if (base == 2){
						print('(');
						print(base);
						print(')');
					}
				}
				print(ar);
			}

			return ok;
		}

		bool print(bool b);
		bool print(char ch);
		bool print(const char* str);

		template<typename T>
		Terminal& operator<<(T t) {
			print(t);
			return *this;
		}

	private:

		bool addSymbol(Symbol sym);
		void parseCmd(void);
		const Command* findCmd(std::string_view input);
		void processSymbol(Symbol sym);

		static constexpr Symbol NewLine = 0x0d;
		static constexpr Symbol NewLine2 = 0x0a;

		ReadSymbol readSymbol {nullptr};
		WriteSymbol writeSymbol {nullptr};

		String<InputStringLengthMax> inputString;
		size_t inputStringIndex {0};

		size_t cmdQty {0};
		const Command* commandTable {nullptr};
};

} //namespace azn

