#pragma once
#include <string_view>
#include <charconv>

namespace azn::bmterm {

struct TokenPos {
	std::size_t begin;
	std::size_t end;
};

TokenPos findToken(std::string_view input, std::size_t start = 0);
void skipToken(std::string_view& input);
bool parse(std::string_view input, char& val);
bool parse(std::string_view input, bool& val);
TokenPos findArray(std::string_view input);
bool isTokenPosOk(const TokenPos& pos);
void skipArray(std::string_view& input);

template<typename T>
bool parse(std::string_view input, T& val) {
	auto tokenPos = findToken(input);
	std::string_view token(input.data() + tokenPos.begin, tokenPos.end - tokenPos.begin);

	int base;
	if ((token.size() >=2) && (token[0] == '0') && (token[1] == 'x')) {
		base = 16;
		token.remove_prefix(2);
	}
	else {
		base = 10;
	}
	auto res = std::from_chars(token.data(), token.data() + token.size(), val, base);
	bool ok = (res.ec != std::errc::invalid_argument) && (res.ec != std::errc::result_out_of_range);
	return ok;
}

template<typename T>
bool parseArray(std::string_view input, T* array, std::size_t cap, std::size_t& size) {
	auto arrayPos = findArray(input);
	bool ok = isTokenPosOk(arrayPos);

	size = 0;
	std::string_view ar(input.data() + arrayPos.begin, arrayPos.end - arrayPos.begin);
	while (ok && (arrayPos.begin < (arrayPos.end - 1))) {
		auto tokenPos = findToken(ar, arrayPos.begin);
		if (tokenPos.begin != tokenPos.end) {
			if (size < cap) {
				std::string_view token(ar.data() + tokenPos.begin, tokenPos.end - tokenPos.begin);
				ok = parse(token, array[size]);
				arrayPos.begin = tokenPos.end;
			}
			else {
				ok = false;
			}
			size++;
		}
		else {
			arrayPos.begin = arrayPos.end;
		}
	}

	return ok;
}

} // azn::bmterm
