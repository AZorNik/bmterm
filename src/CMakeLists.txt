add_library(bmterm_lib STATIC)

target_sources(bmterm_lib
	PUBLIC
		./bmterm.hpp
		./parser.hpp
	PRIVATE
		./bmterm.cpp
		./parser.cpp
)

target_include_directories(bmterm_lib
	PUBLIC
		../include
)

set(script ${PROJECT_SOURCE_DIR}/scripts/genFiles.py)
set_target_properties(bmterm_lib
	PROPERTIES generator ${script}
)


function(bmterm_gen)
	set(oneVal TARGET OUT)
	set(multiVal FILES)
	cmake_parse_arguments(bmgen "" "${oneVal}" "${multiVal}" ${ARGN})
	set(bmgen_DIR ${CMAKE_CURRENT_BINARY_DIR})

	# generate command parsers and table during build configuration
	find_package(Python3 REQUIRED)
	get_target_property(gen bmterm_lib generator)
	execute_process(
		COMMAND ${Python3_EXECUTABLE} ${gen} ${bmgen_FILES}
		WORKING_DIRECTORY ${bmgen_DIR}
	)

  set(generator_output
		${bmgen_DIR}/bmterm_gen/bmterm_parsers.cpp
		${bmgen_DIR}/bmterm_gen/bmterm_table.hpp
	)

  # regenerate parsers and table if any file with commands has been changed
	add_custom_command(
		OUTPUT ${generator_output}
		DEPENDS ${bmgen_FILES}
		COMMAND ${Python3_EXECUTABLE} ${gen} ${bmgen_FILES}
		COMMENT "Re-generate bmterm files for target ${bmgen_TARGET}"
	)

  add_custom_target(bmterm_regenerate
		DEPENDS ${generator_output}
	)

  add_dependencies(${bmgen_TARGET} bmterm_regenerate)
	set(${bmgen_OUT} ${generator_output} PARENT_SCOPE)

	target_include_directories(${bmgen_TARGET}
		PRIVATE
		  ${bmgen_DIR}
	)

endfunction()
