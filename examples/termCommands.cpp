#include "azn/bmterm/bmterm.hpp"
#include <iostream>

//* bmterm cmd "cmd1"
void cmd1(void) {
	std::cout << "cmd1\n";
}

//* bmterm cmd "cmd2 1"
void cmd2(int i) {
	std::cout << "cmd2 " << i << " \n";
}

//* bmterm cmd "cmd3 1[8]"
void cmd3(int* ar, std::size_t n) {
	std::cout << "cmd3 [";
	bool first = true;
	for (std::size_t i = 0; i < n; ++i){
		if (first) {
			first = false;
		}
		else{
			std::cout << ' ';
		}
		std::cout << ar[i];
	}
	std::cout << "]\n";
}


//* bmterm cmd "cmd4 2 1[8]"
void cmd4(int* ar, std::size_t n, char ch) {
	std::cout << "cmd4 [";
	bool first = true;
	for (std::size_t i = 0; i < n; ++i){
		if (first) {
			first = false;
		}
		else{
			std::cout << ch;
		}
		std::cout << ar[i];
	}
	std::cout << "]\n";
}
