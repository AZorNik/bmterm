#include <iostream>
#include "azn/bmterm/bmterm.hpp"
#include "bmterm_gen/bmterm_table.hpp"

using namespace azn::bmterm;

bool doMain = true;
bool terminalRead(Symbol& sym) {
	static const char input[] = "cmd1\n"
															"cmd1\n"
															"cmd2 5\n"
															"cmd3 [1 3 8]\n"
															"cmd3 [7]\n"
															"cmd4  ,  [ -23 -56 ]\n"
															"\n"
															"cmd4  \\  [ -23 -56 ]\n"
															"cmd4 : []\n"
															"cmd5 0\n";
	static size_t i = 0;
	bool gotNewSymbol;
	if (i < sizeof(input)){
		sym = input[i++];
		gotNewSymbol = true;
	}
	else {
		gotNewSymbol = false;
	}

	doMain = gotNewSymbol;
	return gotNewSymbol;
}

bool terminalWrite(const Symbol& sym) {
	std::cout << sym;
	return true;
}

int main(void) {

	Terminal term(terminalRead, terminalWrite);
	term.loadTable(CmdTable, CmdQty);

	while (doMain) {
		term.process();
	}
	return 0;
}
